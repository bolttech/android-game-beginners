package ru.schoolbolt.mycoolapplication;

import android.media.Image;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random random = new Random();

        score = 0;

        TextView score_text = (TextView)findViewById(R.id.scoreText);

        ImageView android_logo = (ImageView)findViewById(R.id.androidLogo);
        ImageView ios_logo = (ImageView)findViewById(R.id.iosLogo);
        ImageView background = (ImageView)findViewById(R.id.background);

        score_text.setText("Score: " + score);

        android_logo.setX(random.nextInt(500));
        android_logo.setY(random.nextInt(500));

        ios_logo.setX(random.nextInt(500));
        ios_logo.setY(random.nextInt(500));

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score = score - 1;
                TextView score_text = (TextView)findViewById(R.id.scoreText);
                score_text.setText("Score: " + score);
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Random random = new Random();

                ImageView android_logo = (ImageView)findViewById(R.id.androidLogo);
                ImageView ios_logo = (ImageView)findViewById(R.id.iosLogo);

                android_logo.setX(random.nextInt(500));
                android_logo.setY(random.nextInt(500));

                ios_logo.setX(random.nextInt(500));
                ios_logo.setY(random.nextInt(500));

                handler.postDelayed(this, 2000);
            }
        }, 2000);

        android_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score = score + 1;
                TextView score_text = (TextView)findViewById(R.id.scoreText);
                score_text.setText("Score: " + score);
            }
        });

        ios_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score = score - 5;
                TextView score_text = (TextView)findViewById(R.id.scoreText);
                score_text.setText("Score: " + score);
            }
        });
    }
}
